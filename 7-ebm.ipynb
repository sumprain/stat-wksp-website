{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "hourly-adaptation",
   "metadata": {},
   "source": [
    "# Evidence Based Medicine: Quality and Strength of Evidence"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "improved-photography",
   "metadata": {},
   "source": [
    "The present and following lectures aim to make us understand the fundamentals of evidence based medicine and clinical research methodology, so that we are able to critically appraise the available evidences. The materials will also enable us to understand the fundamentals of conduct of association studies (observational and experimental)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "affected-component",
   "metadata": {},
   "source": [
    "## What is Evidence Based Medicine (EBM)\n",
    "\n",
    "EBM is a term coined in 1990 by Gordon Guyatt of McMaster University to describe **conscientious, explicit and judicious** use of **current best evidence** in making decisions about care of **individual** patients."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "royal-southwest",
   "metadata": {},
   "source": [
    "Practicing EBM has **three pillars**.\n",
    "\n",
    "1. Presence of **Evidence**\n",
    "\n",
    "2. **Expertise** in carrying out the best intervention as per the evidence\n",
    "\n",
    "3. **Patient** preference\n",
    "\n",
    "We tend to forget the points 2 and 3 when we are practicing clinical medicine."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "approximate-mortgage",
   "metadata": {},
   "source": [
    "## Steps in practicing EBM"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "technical-pilot",
   "metadata": {},
   "source": [
    "1. Develop **research question** (based on daily clinical practice)\n",
    "\n",
    "2. **Search of present literature** for quality and strength of evidence\n",
    "\n",
    "3. **If** literature has good quality and strong evidence, **act according to the evidence**.\n",
    "\n",
    "4. **Else**, plan, design, conduct **clinical study** aimed to answer the question.\n",
    "\n",
    "5. Develop **new evidence**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "broke-professional",
   "metadata": {},
   "source": [
    "## What is Evidence?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "boring-stewart",
   "metadata": {},
   "source": [
    "**Evidence** is the evidence of the **clinically significant effect size** from **research studies**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fantastic-visiting",
   "metadata": {},
   "source": [
    "## Effect size\n",
    "\n",
    "Effect size has already been discussed in previous lectures ([here](3-inferential-stats.ipynb#effect), [here](4-continuous.ipynb#effect), [here](5-binary.ipynb#effect) and [here](6-survival.ipynb#effect)). In nutshell, it is the measure of underlying probabilistic process which is estimated by random sample obtained by carrying out clinical research studies.\n",
    "\n",
    "**Understanding effect** and its **clinically relevant magnitude and direction** is vital for the success of EBM practice and decision making."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "posted-depth",
   "metadata": {},
   "source": [
    "## Properties of evidence"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "therapeutic-playing",
   "metadata": {},
   "source": [
    "There are two properties of evidence based on which effect size is assessed for its correctness and applicability\n",
    "\n",
    "1. **Quality** of evidence: It is authenticity of estimated effect against real effect (which is unknown to us). There are two parts of it: **internal validity** and **external validity**. \n",
    "\n",
    "    a. **Internal validity** looks into the presence of **bias**, which is the systematic difference between estimated effect and real effect. It is assessed by checking **confounders** and **blinding** (in experimental studies).\n",
    "    \n",
    "    b. **External validity** looks into the applicability of evidence to our patient. It is also called **generalisability**.\n",
    "\n",
    "\n",
    "2. **Strength** of evidence: It is assessed by **noting direction and magnitude** of effect size estimated from the study (and also its precision) and by **comparing** it with clinically relevant effect size."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "standing-helicopter",
   "metadata": {},
   "source": [
    "> As an example, a **blinded randomised control trial** is conducted to compare intervention A with intervention B in achieving remission rates. Intervention A leads to remission rates of 50% and intervention B leads to remission rate of 30%, with **risk difference of 20% (95% CI: 15% - 25%)**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "close-ireland",
   "metadata": {},
   "source": [
    "**Let us assess** quality of effect size estimate:\n",
    "\n",
    "1. **Quality of evidence**: **Internal validity -** looks good as it is blinded, RCT which will take care of most of the confounders. **External validity -** we will have to check baseline patient attributes to decide if our patient matches a typical patient in the trial.\n",
    "\n",
    "2. **Strength of evidence:** If **clinicaly significant risk difference is 10%**, our 95% confidence interval is more extreme to 10%, we can infer that evidence is strong that intervention B is better than intervention A.\n",
    "\n",
    "So, evidence is of **good quality and is strong** to infer that intervention B is better than intervention A."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mediterranean-cartoon",
   "metadata": {},
   "source": [
    "## Types of clinical studies\n",
    "\n",
    "There are following types of clinical research studies:\n",
    "\n",
    "1. **Prevalence studies:** These aim to estimate prevalence rates of a clinical outcome.\n",
    "\n",
    "2. **Association studies:** These aim to assess association between exposure variables and outcome variable. The studies can be observational ones (cohort and case control) or experimental ones (randomised trials)\n",
    "\n",
    "3. **Cause Effect studies:** These aim to assess cause effect relation between exposure and outcome variables. The studies are experimental ones (randomised trials)\n",
    "\n",
    "4. **Predictive studies:** These aim to predict future occurrence of an event for a given set of predictive variables.\n",
    "\n",
    "5. **Diagnostic studies:** These aim to assess performance of diagnostic tests.\n",
    "\n",
    "#### For rest of lectures we will be discussing about association and cause effect studies."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "divided-ceramic",
   "metadata": {},
   "source": [
    "## Quality of evidence in association studies\n",
    "\n",
    "Following is the hierarchy of studies with **increasing quality of evidence**:\n",
    "\n",
    "1. Case series\n",
    "\n",
    "2. Case control study\n",
    "\n",
    "3. Cohort study\n",
    "\n",
    "4. Cohort study with analysis adjusted for maximum number of confounders\n",
    "\n",
    "5. Randomised clinical study\n",
    "\n",
    "6. Systematic Review\n",
    "\n",
    "Among the association studies, **experimental design (randomised trial)** has the highest quality of evidence."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "stylish-india",
   "metadata": {},
   "source": [
    "## Steps of appraisal of clinical study as a part of EBM practice"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "communist-actor",
   "metadata": {},
   "source": [
    "### 1. A good research question"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "becoming-copyright",
   "metadata": {},
   "source": [
    "Routine clinical practice forces us to make multiple decisions for a patient.\n",
    "\n",
    "A research question arises from the uncertainty we face in taking any of the decision.\n",
    "\n",
    "Research question is to be developed in a formal and deliberate way and following points (**PICOT**) should be included in it:\n",
    "\n",
    "1. **P**opulation characteristics\n",
    "\n",
    "2. **I**ntervention to be given\n",
    "\n",
    "3. **C**ontrol population\n",
    "\n",
    "4. **O**utcome to be assessed\n",
    "\n",
    "5. **T**ime to follow up\n",
    "\n",
    "All the above components need to be carefully designed and defined before start of the study, in case we are planning to conduct the study, or before start of literature search, if we aim to gather evidence for the particular research question."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "environmental-desperate",
   "metadata": {},
   "source": [
    "### 2. Assess generalizability of the study conduct on the present patient\n",
    "\n",
    "Following are to be assessed:\n",
    "\n",
    "1. Present patient is a **typical patient** in the clinical study in question\n",
    "\n",
    "2. We are **capable to provide intervention** provided in the clinical study\n",
    "\n",
    "3. The **follow up environment** (like supportive care, environment in which patient lives, etc) is similar to that of the clinical study."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "detected-establishment",
   "metadata": {},
   "source": [
    "### 3. Check for adequacy of sample size\n",
    "\n",
    "Following are to be checked:\n",
    "\n",
    "1. **Clinically relevant effect size chosen by the investigators.** We have to check if the clinically relevant effect size is relevant for us.\n",
    "\n",
    "2. Adequacy of **power** of study.\n",
    "\n",
    "3. **Correctness of underlying probability distribution** chosen for the sample size calculation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "smooth-flood",
   "metadata": {},
   "source": [
    "### 4. Check for baseline characteristics of patients\n",
    "\n",
    "Baseline characteristics of patients serve two purposes:\n",
    "\n",
    "1. **Describes a typical patient** in the clinical study.\n",
    "\n",
    "2. Provides credential to the study by **displaying equality of potential confounders** among intervention arms in association studies.\n",
    "\n",
    "#### It serves no purpose to apply statistical tests to check for equality of baseline characteristics among intervention arms."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "copyrighted-airport",
   "metadata": {},
   "source": [
    "### 5. Check for randomisation strategy (for experimental designs)\n",
    "\n",
    "### 6. Check for adequacy of concealment at various relevant stages of the study (for experimental designs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "wooden-fraction",
   "metadata": {},
   "source": [
    "### 7. Check for adequate handling of confounders and presence of bias\n",
    "\n",
    "### 8. Check direction and magnitude of estimate of effect size and compare with clinically relevant effect size\n",
    "\n",
    "### 9. Take informed clinical decision (as per the EBM)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
