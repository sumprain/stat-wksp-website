import matplotlib.pyplot as plt
from statsmodels.graphics.gofplots import qqplot

def QQplot (x, **kwargs):
    ax = plt.gca()
    if 'color' in kwargs:
        kwargs['markerfacecolor'] = kwargs['color']
    qqplot(x, ax = ax, **kwargs)
    plt.gca().lines[-1].set_color(kwargs['color'])
